defmodule Grains.Support do
  @moduledoc """
  Support functions to simplify testing.
  """

  alias Grains.Support.TestSetup

  @doc """
  Generate boiler plate to blackbox-test a grain.

  This helper starts a supervised setup to test a "Grain under Test" (GuT):

  ```
  Publisher ---> GuT ---> Subscriber
  ```

  By publishing messages through the `Publisher`, GuT can be blackbox-tested:
  the `Subscriber` forwards all messages to the process that called `setup_test!/2`.

  Use `push_into_test/2` to publish a message to GuT.

  ## Arguments

  * `module`: The module of the GuT
  * `args`: The arguments to initialize a GuT

  """
  def setup_test!(module, args) when is_atom(module) and is_list(args) do
    recipe =
      Grains.Recipe.new(:Test, %{
        :Publisher => :GrainUnderTest,
        :GrainUnderTest => :Subscriber
      })

    grains =
      Grains.new(%{
        :Publisher => {Grains.Support.Publisher, [], []},
        :GrainUnderTest => {module, args, []},
        :Subscriber => {Grains.Support.Subscriber, [subscriber: self()], []}
      })

    {:ok, bread} = Grains.start_supervised(recipe, grains)

    %TestSetup{
      publisher: Grains.get_name(bread, :Publisher),
      subscriber: Grains.get_name(bread, :Subscriber),
      bread: bread
    }
  end

  @doc """
  Publish a message to the "Grain under Test"

  ## Example

      iex> test_setup = setup_test!(Grains.Support.Cache, [])
      iex> test_setup |> push_into_test(1) |> push_into_test(2)
      iex> assert_receive 1
      1
      iex> assert_receive 2
      2

  """
  def push_into_test(test_setup = %TestSetup{}, message) do
    GenServer.cast(test_setup.publisher, message)

    test_setup
  end
end
