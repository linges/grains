defmodule Grains.Support.TestSetup do
  @moduledoc false

  @enforce_keys [:publisher, :subscriber, :bread]

  defstruct @enforce_keys
end
