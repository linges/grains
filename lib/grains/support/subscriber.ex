defmodule Grains.Support.Subscriber do
  @moduledoc """
  A grain to pull message and forward pushed messages to a subscribing process.

  This grain is meant for testing purposes, where the incoming messages
  should be forwarded to the test process.
  """

  use Grains.GenGrain

  defmodule State do
    @enforce_keys [:subscriber]
    defstruct @enforce_keys ++ [with_tag: false]
  end

  @impl true
  def init(args) do
    {:ok, struct!(State, args)}
  end

  @impl true
  def handle_push(msg, _from, state) do
    send(state.subscriber, msg)
    {:noreply, state}
  end

  @impl true
  def handle_cast(:pull, state = %State{with_tag: false}) do
    pull()
    {:noreply, state}
  end

  def handle_cast(:pull, state = %State{with_tag: tag}) do
    pull_with_tag(tag)
    {:noreply, state}
  end

  def handle_cast({:pull, tag}, state) do
    pull_with_tag(tag)
    {:noreply, state}
  end
end
