defmodule Grains.Timer do
  @moduledoc """
  A periodic timer.
  """

  use Grains.GenGrain

  defmodule State do
    @enforce_keys [:period]
    defstruct @enforce_keys ++ [with_timestamps: false, with_tag: false]
  end

  def name(left, grains) do
    grains = Enum.sort(grains)

    [left, Timer, grains]
    |> List.flatten()
    |> Module.concat()
  end

  def init(opts) do
    state = struct!(State, opts)
    {:ok, _} = :timer.send_interval(state.period, :tick)

    {:ok, state}
  end

  def handle_info(:tick, state = %State{with_tag: false}) do
    pull()

    {:noreply, state}
  end

  def handle_info(:tick, state = %State{with_tag: tag}) do
    pull_with_tag(tag)

    {:noreply, state}
  end

  def handle_push(msg, _from, state = %{with_timestamps: true}) do
    now = DateTime.utc_now()
    :ok = push({now, msg})
    {:noreply, state}
  end

  def handle_push(msg, _from, state) do
    :ok = push(msg)
    {:noreply, state}
  end

  def handle_pull(_from, state) do
    pull()
    {:noreply, state}
  end

  def handle_pull(_from, tag, state) do
    pull_with_tag(tag)
    {:noreply, state}
  end
end
