defmodule TestGrain do
  use Grains.GenGrain

  def init(_), do: {:ok, :ok}

  def handle_push(msg, _, state) do
    push(msg)
    {:noreply, state}
  end
end

defmodule AlphaOmega do
  use Grains.GenGrain
  def init(_), do: {:ok, :ok}

  def handle_push(i, _, answer) do
    GenServer.reply(answer, i)
    {:noreply, nil}
  end

  def handle_call(msg, from, _state) do
    push(msg)
    {:noreply, from}
  end

  def bake(n) do
    start = {TestGrain, :foo, []}
    c = String.to_atom("#{0}")

    acc =
    {Grains.new_recipe(:test, %{AO => c}),
     Grains.new(%{c => start, AO => {AlphaOmega, %{}, []}})}

    acc =
      Enum.reduce(1..n, acc, fn i, acc ->
        p = String.to_atom("#{i - 1}")
        c = String.to_atom("#{i}")
        r = Grains.new_recipe(:test, %{p => c})
        g = Grains.new(%{c => start})
        Grains.combine(acc, {r, g})
      end)

    Grains.combine(
      {
        Grains.new_recipe(:test, %{String.to_atom("#{n}") => AO}),
        Grains.new(%{})
      },
      acc
    )
  end

  def id(_, x) do
    x
  end
end

Benchee.run(
  %{
    "grains" => {
    fn {pid, msg} -> GenServer.call(pid, msg) end,
    before_scenario: fn {grains_n, list} ->
      {recipe, grains} = AlphaOmega.bake(grains_n)
      {:ok, sup} = Grains.start_supervised(recipe, grains)
      root = Grains.Supervisor.get_root_name(sup)
      pid = Module.concat([root, AO]) |> Process.whereis()
      {pid, list}
    end
    },
    "fun" => fn {n, list} ->
      Enum.reduce(0..n, list, &AlphaOmega.id/2)
    end
  },
  inputs: %{
    "grains 1 length 10" => {1, Enum.to_list(1..10)},
    "grains 1 length 100" => {1, Enum.to_list(1..100)},
    "grains 10 length 10" => {10, Enum.to_list(1..10)},
    "grains 10 length 100" => {10, Enum.to_list(1..100)},
    "grains 100 length 10" => {100, Enum.to_list(1..10)},
    "grains 100 length 100" => {100, Enum.to_list(1..100)}
  }
)
