defmodule Grains.Support.TransformerTest do
  use ExUnit.Case

  alias Grains.Support.Publisher
  alias Grains.Support.Subscriber
  alias Grains.Support.Transformer
  alias Grains.Support.Trace

  setup do
    recipe =
      Grains.Recipe.new(:pubsub, %{
        :Publisher => :Transformer,
        :Transformer => :Subscriber
      })

    grains =
      Grains.new(%{
        :Publisher => {Publisher, [], []},
        :Transformer => {Transformer, [transform: &transform/1], []},
        :Subscriber => {Subscriber, [subscriber: self()], []}
      })

    {:ok, bread} = Grains.start_supervised(recipe, grains)

    [
      bread: bread,
      publisher: Grains.get_name(bread, :Publisher),
      subscriber: Grains.get_name(bread, :Subscriber)
    ]
  end

  defp transform(:hello) do
    :world
  end

  test "transforms push", ctx do
    GenServer.cast(ctx.publisher, :hello)

    assert_receive :world
  end

  test "handles pull and pull_with_tag", ctx do
    start_supervised(Trace)
    Trace.pull(ctx.bread, target: :Transformer, what: :send)

    GenServer.cast(ctx.subscriber, :pull)
    GenServer.cast(ctx.subscriber, {:pull, :tag})

    assert_receive {:send, :Transformer, :Publisher, {:pull, :Transformer}}
    assert_receive {:send, :Transformer, :Publisher, {:pull, :tag, :Transformer}}
  end
end
