defmodule Grains.Support.ConstantTest do
  use ExUnit.Case

  alias Grains.Support.Subscriber
  alias Grains.Support.Constant
  alias Grains.Support.Utils

  setup do
    recipe =
      Grains.Recipe.new(:pubsub, %{
        :Constant => :Subscriber
      })

    grains =
      Grains.new(%{
        :Constant => {Constant, [constant: :the_constant], []},
        :Subscriber => {Subscriber, [subscriber: self()], []}
      })

    {:ok, sup} = Grains.start_supervised(recipe, grains)

    [sup: sup]
  end

  test "sends constant on push", ctx do
    Utils.inject_push(ctx.sup, :Constant, :Test, :some_message)

    assert_receive :the_constant
  end

  test "sends constant on pull and pull_with_tag", ctx do
    Utils.inject_pull(ctx.sup, :Constant, :Subscriber)
    assert_receive :the_constant

    Utils.inject_pull_with_tag(ctx.sup, :Constant, :Subscriber, :SomeTag)
    assert_receive :the_constant
  end
end
