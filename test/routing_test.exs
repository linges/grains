defmodule RoutingTest do
  use ExUnit.Case

  alias Grains.Recipe
  alias Grains.Supervisor
  alias Grains.GenGrain
  alias Grains.Bread
  alias Grains.Support.Trace
  import Grains

  defmodule TestGrain do
    use GenGrain

    def init(tag) do
      {:ok, tag}
    end

    def handle_info(msg, tag) do
      push(msg)
      {:noreply, tag}
    end

    def handle_push(msg, _from, tag) do
      push(msg)
      {:noreply, tag}
    end
  end

  def start_grains_sup(recipe, grains, args \\ []) do
    spec = %{id: :test_grains_sup, start: {Supervisor, :start_link, [recipe, grains, args]}}
    start_supervised(spec)
  end

  describe "routing" do
    test "simple route" do
      recipe = Recipe.new(:test1, %{A => [B, route(:foo, C)]})

      grains =
        Grains.new(%{
          A => {TestGrain, A, []},
          B => {TestGrain, B, []},
          C => {TestGrain, C, []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains)

      start_supervised!(Trace)
      a = Grains.get_name(sup, A)

      Trace.push(sup, target: B, what: :receive)
      Trace.push(sup, target: C, what: :receive)
      send(a, :foo)
      assert_receive {:receive, B, {:push, A, :foo}}
      assert_receive {:receive, C, {:push, A, :foo}}

      send(a, :bar)
      assert_receive {:receive, B, {:push, A, :bar}}
      refute_receive {:receive, C, {:push, A, :bar}}
    end

    test "multiple routes same grain" do
      recipe =
        Recipe.new(:test1, %{
          # :bar OR :foo OR (%{a: _} AND %{b: _})
          A => [route(:bar, B), route(:foo, B), route(%{a: _}, route(%{b: _}, B))]
        })

      grains =
        Grains.new(%{
          A => {TestGrain, A, []},
          B => {TestGrain, B, []}
        })

      {:ok, sup} = start_grains_sup(recipe, grains)

      start_supervised!(Trace)
      a = Grains.get_name(sup, A)

      Trace.push(sup, target: B, what: :receive)

      # AND routes
      send(a, :foo)
      assert_receive {:receive, B, {:push, A, :foo}}

      send(a, :bar)
      assert_receive {:receive, B, {:push, A, :bar}}

      send(a, nil)
      refute_receive {:receive, B, {:push, A, nil}}

      # OR routes
      send(a, %{a: :a, b: :b})
      assert_receive {:receive, B, {:push, A, _}}

      send(a, %{a: :a, c: :b})
      refute_receive {:receive, B, {:push, A, _}}

      send(a, %{c: :a, b: :b})
      refute_receive {:receive, B, {:push, A, _}}
    end
  end

  describe "baking" do
    test "route of lists" do
      recipe_route = Recipe.new(:test1, %{A => route(:foo, [A, B]), B => C})

      grains =
        Grains.new(%{
          A => {TestGrain, A, []},
          B => {TestGrain, B, []},
          C => {TestGrain, C, []}
        })

      bread_route = Bread.bake(recipe_route, grains, id: :test1)
      :ok = bread_route |> Bread.check()

      assert [{A, A, %{router: [_]}}, {A, B, %{router: [_]}}, {B, C, %{}}] =
               Enum.sort(bread_route.final_recipe)
    end

    test "list of routes" do
      router = fn _ -> true end

      recipe_1 =
        Recipe.new(:test1, %{A => [{:route, router, [A], ""}, {:route, router, [B], ""}]})

      recipe_2 = Recipe.new(:test1, %{A => {:route, router, [A, B], ""}})

      grains =
        Grains.new(%{
          A => {TestGrain, A, []},
          B => {TestGrain, B, []}
        })

      bread_1 = Bread.bake(recipe_1, grains, id: :test1)
      :ok = bread_1 |> Bread.check()

      bread_2 = Bread.bake(recipe_2, grains, id: :test1)
      assert bread_1.final_recipe == bread_2.final_recipe
    end

    test "route of one" do
      recipe_route = Recipe.new(:test1, %{A => route(:foo, B)})

      grains =
        Grains.new(%{
          A => {TestGrain, A, []},
          B => {TestGrain, B, []}
        })

      bread = Bread.bake(recipe_route, grains)
      :ok = bread |> Bread.check()
      assert [{A, B, %{router: [_], router_string: [":foo"]}}] = bread.final_recipe
    end

    test "route with periodic" do
      recipe_route =
        Recipe.new(:test1, %{
          A =>
            route(
              :foo,
              periodic(
                [E, route(:bar, [B, route(:c, [C, D])])],
                1000
              )
            ),
          B => C
        })

      grains =
        Grains.new(%{
          A => {TestGrain, A, []},
          B => {TestGrain, B, []},
          C => {TestGrain, C, []},
          D => {TestGrain, C, []},
          E => {TestGrain, E, []}
        })

      bread = Bread.bake(recipe_route, grains)
      :ok = bread |> Bread.check()

      timer = Grains.Timer.name(A, [E, B, C, D])

      assert [
               {A, ^timer, %{router_string: [":foo"]}},
               {^timer, B, %{router_string: [":bar"]}},
               {^timer, C, %{router_string: [":bar", ":c"]}},
               {^timer, D, %{router_string: [":bar", ":c"]}},
               {^timer, E, _},
               {B, C, _}
             ] = Enum.sort(bread.final_recipe)
    end
  end

  describe "route macro" do
    test "simple route with list" do
      {:route, router, pred, ":foo"} = route(:foo, [A, B])
      assert is_function(router, 1)
      assert pred == [A, B]
      assert router.(:foo)
      assert not router.(:bar)
    end

    test "simple route without list" do
      {:route, _router, pred, _} = route(:foo, A)
      assert pred == [A]
    end

    test "complex route" do
      {:route, router, _pred, "%{foo: _, bar: x} when x == 3"} =
        route(%{foo: _, bar: x} when x == 3, [A, B])

      assert is_function(router, 1)
      assert router.(%{foo: 3, bar: 3})
      assert router.(%{foo: 3, bar: 3, x: :y})
      assert not router.(%{foo: 3, bar: 2})
      assert not router.(%{bar: 3})
    end

    test "route in recipe" do
      recipe = Recipe.new(:test1, %{A => B, B => route(:foo, C)})
      assert %Recipe{name: :test1, map: %{A => B, B => {:route, router, [C], _}}} = recipe
      assert router.(:foo)
      assert not router.(:bar)
    end

    test "list routes" do
      assert [{:route, _, [A], _}, {:route, _, [B, C], _}] = route([{:foo, A}, {:bar, [B, C]}])
    end

    test "route with periodic" do
      assert {:route, _, [{:periodic, _, _}], _} = route(:foo, periodic(A, 1000))
    end
  end
end
