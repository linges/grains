defmodule Grains.TimerTest do
  use ExUnit.Case

  defmodule ConstantWithTag do
    use Grains.GenGrain

    defstruct [:constant]

    def init(args) do
      {:ok, struct!(__MODULE__, args)}
    end

    def handle_pull(_from, tag, state) do
      {:reply, {tag, state.constant}, state}
    end
  end

  test "Timer maintains correct tag on pull" do
    recipe =
      Grains.new_recipe(Test, %{
        :Constant => Grains.periodic(:Subscriber, 1_000, %{})
      })

    grains =
      Grains.new(%{
        :Constant => {ConstantWithTag, %{constant: :hello}, []},
        :Subscriber => {Grains.Support.Subscriber, %{subscriber: self()}, []}
      })

    {:ok, bread} = Grains.start_supervised(recipe, grains)

    Grains.Support.Utils.inject_pull_all_with_tag(bread, :Subscriber, :my_tag)

    assert_receive {:my_tag, :hello}
  end

  test "DebugTimer maintains correct tag on pull" do
    recipe =
      Grains.new_recipe(Test, %{
        :Constant => Grains.periodic(:Subscriber, 1_000, %{})
      })

    grains =
      Grains.new(%{
        :Constant => {ConstantWithTag, %{constant: :hello}, []},
        :Subscriber => {Grains.Support.Subscriber, %{subscriber: self()}, []},
        Constant.Timer.Subscriber => {Grains.DebugTimer, %{}, []}
      })

    {:ok, bread} = Grains.start_supervised(recipe, grains, id: :Test)

    Grains.Support.Utils.inject_pull_all_with_tag(bread, :Subscriber, :my_tag)

    assert_receive {:my_tag, :hello}
  end
end
