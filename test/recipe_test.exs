defmodule Grains.RecipeTest do
  use ExUnit.Case

  alias Grains.Recipe

  test "to_mermaid/1,2" do
    assert "graph TD" ==
             Test
             |> Recipe.new(%{})
             |> Recipe.to_mermaid()

    assert "flowchart TD" ==
             Test
             |> Recipe.new(%{})
             |> Recipe.to_mermaid(type: "flowchart")

    assert "graph TB" ==
             Test
             |> Recipe.new(%{})
             |> Recipe.to_mermaid(direction: "TB")

    ## Merging Edges

    require Grains

    recipe = %{
      Producer => [Consumer]
    }

    assert "graph TD \n" <>
             "Producer --> Consumer" ==
             Test
             |> Recipe.new(recipe)
             |> Recipe.to_mermaid(merge_edges: false)

    recipe = %{
      Producer => [
        Grains.route(:some_message, [Consumer]),
        Grains.route(:another_message, Grains.periodic(Consumer, 1000))
      ]
    }

    assert "graph TD \n" <>
             "Producer -->|\":some_message\"| Consumer \n" <>
             "Producer -->|\":another_message, periodic\"| Consumer" ==
             Test
             |> Recipe.new(recipe)
             |> Recipe.to_mermaid(merge_edges: false)

    assert "graph TD \n" <>
             "Producer -->|\":another_message, periodic<br />:some_message\"| Consumer" ==
             Test
             |> Recipe.new(recipe)
             |> Recipe.to_mermaid(merge_edges: true)

    ## Ghost Edges
    assert "graph TD \n" <>
             "A -.-> B \nA -.->|\"description\"| C" ==
             Test
             |> Recipe.new(%{A: [Grains.ghost(B), Grains.ghost(C, "description")]})
             |> Recipe.to_mermaid(merge_edges: false)
  end
end
