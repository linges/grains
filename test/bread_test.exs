defmodule Grains.BreadTest do
  use ExUnit.Case

  alias Grains.Bread
  alias Grains.Recipe
  require Grains

  setup do
    recipe =
      Recipe.new(Test, %{
        A => [Grains.route(msg when is_tuple(msg), B), C],
        B => D,
        C => D,
        D => Grains.ghost(C)
      })

    grains =
      Grains.new(%{
        A => {DoesNotExist, [], []},
        B => {DoesNotExist, [], []},
        C => {DoesNotExist, [], []},
        D => {DoesNotExist, [], []}
      })

    [bread: Bread.bake(recipe, grains)]
  end

  test "successors/2", ctx do
    assert [C, B] == Bread.successors(ctx.bread, A)
    assert [D] == Bread.successors(ctx.bread, B)
    assert [D] == Bread.successors(ctx.bread, C)
    assert [] == Bread.successors(ctx.bread, D)
  end

  test "predecessors/2", ctx do
    assert [] == Bread.predecessors(ctx.bread, A)
    assert [A] == Bread.predecessors(ctx.bread, B)
    assert [A] == Bread.predecessors(ctx.bread, C)
    assert [C, B] == Bread.predecessors(ctx.bread, D)
  end

  test "ghosts/2", ctx do
    assert [] == Bread.ghosts(ctx.bread, A)
    assert [] == Bread.ghosts(ctx.bread, B)
    assert [] == Bread.ghosts(ctx.bread, C)
    assert [C] == Bread.ghosts(ctx.bread, D)
  end

  test "routes/2", ctx do
    assert [{B, [fun]}] = Bread.routes(ctx.bread, A)
    assert is_function(fun)
    assert [] == Bread.routes(ctx.bread, B)
    assert [] == Bread.routes(ctx.bread, C)
    assert [] == Bread.routes(ctx.bread, D)
  end

  test "paths_between/3", ctx do
    assert [[A, B, D], [A, C, D]] == Bread.paths_between(ctx.bread, A, D)
    assert [] == Bread.paths_between(ctx.bread, A, DoesNotExist)
    assert [] == Bread.paths_between(ctx.bread, DoesNotExist, D)
  end

  test "paths_between/3 with edge C-->B" do
    recipe = Recipe.new(Test, %{A => [B, C], B => [C, D], C => D})

    grains =
      Grains.new(%{
        A => {DoesNotExist, [], []},
        B => {DoesNotExist, [], []},
        C => {DoesNotExist, [], []},
        D => {DoesNotExist, [], []}
      })

    bread = Bread.bake(recipe, grains)

    expected = [[A, B, D], [A, C, D], [A, B, C, D]]
    assert canonic(expected) == canonic(Bread.paths_between(bread, A, D))
  end

  test "paths_between/3 with cycle" do
    recipe = Recipe.new(Test, %{A => [B, C], B => [A, B, C, D], C => [B, D], D => A})

    grains =
      Grains.new(%{
        A => {DoesNotExist, [], []},
        B => {DoesNotExist, [], []},
        C => {DoesNotExist, [], []},
        D => {DoesNotExist, [], []}
      })

    bread = Bread.bake(recipe, grains)

    expected = [[A, B, D], [A, C, D], [A, B, C, D], [A, C, B, D]]
    assert canonic(expected) == canonic(Bread.paths_between(bread, A, D))
  end

  test "paths_between/3 with nodes outside of any path" do
    recipe = Recipe.new(Test, %{A => [B, C], B => D})

    grains =
      Grains.new(%{
        A => {DoesNotExist, [], []},
        B => {DoesNotExist, [], []},
        C => {DoesNotExist, [], []},
        D => {DoesNotExist, [], []}
      })

    bread = Bread.bake(recipe, grains)

    assert [[A, B, D]] == Bread.paths_between(bread, A, D)
  end

  test "paths_between/3 with shortcuts" do
    recipe = Recipe.new(Test, %{A => [B, C, E], B => D, C => D, D => E})

    grains =
      Grains.new(%{
        A => {DoesNotExist, [], []},
        B => {DoesNotExist, [], []},
        C => {DoesNotExist, [], []},
        D => {DoesNotExist, [], []}
      })

    bread = Bread.bake(recipe, grains)

    expected = [[A, B, D, E], [A, C, D, E], [A, E]]

    assert canonic(expected) == canonic(Bread.paths_between(bread, A, E))
  end

  test "child specs are created according to order" do
    recipe = Recipe.new(Test, %{A => [B, C], B => [C, D], C => D})

    grains =
      Grains.new(%{
        A => {DoesNotExist, [], [], 2},
        B => {DoesNotExist, [], [], 3},
        C => {DoesNotExist, [], [], 1},
        D => {DoesNotExist, [], [], 4}
      })

    bread = Bread.bake(recipe, grains)

    assert bread.child_specs |> Enum.map(&short_name/1) == [C, A, B, D]
  end

  defp short_name(spec) do
    [_, _, name] = Module.split(spec.id)
    Module.concat([name])
  end

  defp canonic(paths) do
    Enum.sort(paths)
  end
end
