defmodule Grains.SupportTest do
  use ExUnit.Case

  alias Grains.Support

  doctest Support, import: true

  defmodule Forwarder do
    use Grains.GenGrain

    @impl true
    def init(_) do
      {:ok, :ok}
    end

    @impl true
    def handle_push(msg, _from, state) do
      push(msg)
      {:noreply, state}
    end
  end

  test "setup_test! and push_into_test" do
    Forwarder
    |> Support.setup_test!([])
    |> Support.push_into_test(:test_message)

    assert_receive :test_message
  end

  test "setup_test! exports bread" do
    test_setup = Forwarder |> Support.setup_test!([])

    assert bread = test_setup.bread
    assert is_pid(bread)
  end

  test "setup_test! exports publisher and subscriber" do
    test_setup = Forwarder |> Support.setup_test!([])

    assert publisher = test_setup.publisher
    assert subscriber = test_setup.subscriber
    assert publisher != subscriber
  end
end
