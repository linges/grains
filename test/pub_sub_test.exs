defmodule PubSubTest do
  # Test the support helpers for publishing and subscribing
  use ExUnit.Case

  alias Grains.Recipe

  alias Grains.Support.Cache
  alias Grains.Support.Publisher
  alias Grains.Support.Subscriber

  setup do
    recipe =
      Recipe.new(:pubsub, %{
        :Publisher => :Cache,
        :Cache => :Subscriber
      })

    grains =
      Grains.new(%{
        :Publisher => {Publisher, [], []},
        :Cache => {Cache, [], []},
        :Subscriber => {Subscriber, [subscriber: self()], []}
      })

    {:ok, bread} =
      start_supervised(%{id: :pubsub, start: {Grains.Supervisor, :start_link, [recipe, grains]}})

    publisher = Grains.get_name(bread, :Publisher)
    cache = Grains.get_name(bread, :Cache)
    subscriber = Grains.get_name(bread, :Subscriber)

    [publisher: publisher, cache: cache, subscriber: subscriber]
  end

  test "receives published message", ctx do
    assert :no_value_received_yet == Grains.get_substate(ctx.cache)

    assert :ok == GenServer.cast(ctx.publisher, :hello)
    assert_receive :hello

    assert :hello == Grains.get_substate(ctx.cache)

    assert :ok == GenServer.cast(ctx.publisher, :world)
    assert_receive :world

    assert :world == Grains.get_substate(ctx.cache)

    refute_receive _

    # Retrieve the cached value
    GenServer.cast(ctx.subscriber, :pull)
    assert :world == Grains.get_substate(ctx.cache)
  end
end
