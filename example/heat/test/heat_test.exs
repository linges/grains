defmodule HeatTest do
  use ExUnit.Case

  describe "State" do
    alias Heat.State

    setup do
      {_stage, state} = State.init([])

      [state: state]
    end

    test "set and get target temperature", ctx do
      assert {:reply, :ok, state} = State.handle_call({:set_target_temperature, 10.0}, :ignored, ctx.state)
      assert {:reply, 10.0, _state} = State.handle_call(:get_target_temperature, :ignored, state)
    end

    test "update heater temperature", ctx do
      assert {:noreply, state} = State.handle_info({:update, 1, 30.0}, ctx.state)
      assert 30.0 = state.heaters[1]
    end
  end

  describe "Heater" do
    alias Heat.Heating

    setup do
      serial = 1
      server = self()

      {:ok, state} = Heating.init([serial, server])

      [state: state]
    end

    test "initial temperature", ctx do
      assert 20.0 == ctx.state.temperature
    end

    test "sends ticks to server", ctx do
      serial = ctx.state.serial
      assert_receive :tick
      {:noreply, _state} = Heating.handle_info(:tick, ctx.state)
      assert_receive {:update, ^serial, _temp}
    end

    test "can set action", ctx do
      {:noreply, state} = Heating.handle_info({:action, :heat}, ctx.state)

      assert state.action == :heat
    end

    test "ticks mutates temperature", ctx do
      {:noreply, state1} = Heating.handle_info(:tick, ctx.state)
      {:noreply, state2} = Heating.handle_info(:tick, state1)
      {:noreply, state3} = Heating.handle_info(:tick, state2)

      assert state1.temperature > state2.temperature
      assert state2.temperature > state3.temperature
    end

    test "heating results in temperature increase", ctx do
      {:noreply, state} = Heating.handle_info({:action, :heat}, ctx.state)

      {:noreply, state1} = Heating.handle_info(:tick, state)
      {:noreply, state2} = Heating.handle_info(:tick, state1)
      {:noreply, state3} = Heating.handle_info(:tick, state2)

      assert state1.temperature < state2.temperature
      assert state2.temperature < state3.temperature
    end
  end
end
