* [ ] Bump version number
    * in `mix.exs`
    * adapt `CHANGELOG.md`
* [ ] Commit changes
* [ ] `git tag` version
* [ ] `mix hex.publish --dry-run` and check if this looks ok
* [ ] `git push` and `git push --tags`
* [ ] `mix hex.publish`
